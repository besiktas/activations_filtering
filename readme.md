# activations explorer


# to use

look at `config.py` and change

1. generate a model with `python train_model.py`
2. create adversarial examples with `python generate_adversarial.py n` where n is the size of the random subset of val images you want to use
    - Note: this takes a super long time (each image is more than 1 minute i think?)
3. create activations/