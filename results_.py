import numpy as np

import config



x_val_activations = np.load(config.SAVED_DATA_PATH / "activations" / "x_val_activations.npy", allow_pickle=True)
cw_activations = np.load(config.SAVED_DATA_PATH / "activations" / "cw_activations.npy", allow_pickle=True)
fgsm_activations = np.load(config.SAVED_DATA_PATH / "activations" / "fgsm_activations.npy", allow_pickle=True)


x_val_embeddings = np.load(config.SAVED_DATA_PATH / "embeddings" / "x_val_embeddings.npy", allow_pickle=True)
cw_x_val_emeddings = np.load(config.SAVED_DATA_PATH / "embeddings" / "cw_x_val_emeddings.npy", allow_pickle=True)
fgsm_x_val_emeddings = np.load(config.SAVED_DATA_PATH / "embeddings" / "fgsm_x_val_emeddings.npy", allow_pickle=True)


x_val = np.load(config.SAVED_DATA_PATH / "original" / "x_val.npy")
x_val_split = np.load(config.SAVED_DATA_PATH / "original" / "x_val_split.npy")
y_val = np.load(config.SAVED_DATA_PATH / "original" / "y_val.npy")
y_val_split = np.load(config.SAVED_DATA_PATH / "original" / "y_val_split.npy")

num_classes = y_val.shape[1]
x_val_activations.shape[1]

y_val = y_val.argmax(axis=1)
y_val_split = y_val_split.argmax(axis=1)

cw_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "cw_x_val.npy")
fgsm_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "fgsm_x_val.npy")

cw_x_val = cw_x_val.squeeze()
fgsm_x_val = fgsm_x_val.squeeze()


# might want just subset
# # taking a subset here
# cw_x_val = cw_x_val[:100]
# fgsm_x_val = fgsm_x_val[:100]
# x_val = x_val[:100]
# x_val_split = x_val_split[:100]
# y_val = y_val[:100]
# y_val_split = y_val_split[:100]

