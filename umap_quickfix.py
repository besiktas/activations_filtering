import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
import tensorflow as tf
from tqdm import tqdm, trange

import config

import umap

print(f"=== === === ===")
print(f"=== === === ===")
print(
    f"==>NOTE: THIS STUFF SHOULD BE PART OF `generate_activations.py` \nBUT IS HERE BECAUSE GPU WASNT WORKING SO JUST DOING EMBEDDINGS STUFF WHICH WORKS ON CPU"
)
print(f"=== === === ===")
print(f"=== === === ===")

num_classes = 10
num_layers = 156


x_val = np.load(config.SAVED_DATA_PATH / "original" / "x_val.npy")
x_val_split = np.load(config.SAVED_DATA_PATH / "original" / "x_val_split.npy")
y_val = np.load(config.SAVED_DATA_PATH / "original" / "y_val.npy")
y_val_split = np.load(config.SAVED_DATA_PATH / "original" / "y_val_split.npy")
cw_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "cw_x_val.npy")
fgsm_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "fgsm_x_val.npy")

cw_x_val = cw_x_val.squeeze()
fgsm_x_val = fgsm_x_val.squeeze()

# only take as many as we have embeddings shape
N_SUBSET = config.OOM_SAMPLE_SIZE
cw_x_val = cw_x_val[:N_SUBSET]
fgsm_x_val = fgsm_x_val[:N_SUBSET]
x_val = x_val[:N_SUBSET]
x_val_split = x_val_split[:N_SUBSET]
y_val = y_val[:N_SUBSET]
y_val_split = y_val_split[:N_SUBSET]


def compute_centroid(activations, n_sigma=2.0):
    # filter outliers
    filtered_activations = activations[
        (abs(activations[:, 0] - np.mean(activations[:, 0])) < n_sigma * np.std(activations[:, 0]))
        & (abs(activations[:, 1] - np.mean(activations[:, 1])) < n_sigma * np.std(activations[:, 1]))
    ]

    centroid = filtered_activations.mean(axis=0)
    std = filtered_activations.std(axis=0)
    return centroid, std


# === === === ===
# === === === ===
# BELOW IS FOR REMAKING EMBEDDINGS
# === === === ===
# === === === ===

# get_preds = lambda arr: np.argmax([*arr], axis=1)
transform_for_umap = lambda data: np.array([*data]).reshape(data.shape[0], -1)
umap_arr = {val: None for val in range(num_layers)}


def make_umap_arr(activations, class_idxs):
    umap_arr = []
    embeddings = [[] for _ in range(activations.shape[0])]
    # for layer_n in range(num_layers):
    for layer_n in trange(num_layers):
        umap_obj = umap.UMAP(
            n_components=2,
            min_dist=0.0,
            # random_state=42,
            #         transform_queue_size=6
        )
        _ = umap_obj.fit_transform(transform_for_umap(activations[:, layer_n]))
        umap_arr.append(umap_obj)

        centroids = {val: [] for val in range(num_classes)}
        stds = {val: [] for val in range(num_classes)}

        for class_n in range(num_classes):
            class_activations = transform_for_umap(activations[class_idxs[class_n], layer_n])
            class_embeddings = umap_obj.transform(class_activations)

            centroid, std = compute_centroid(class_embeddings)

            centroids[class_n].append(centroid)
            stds[class_n].append(std)

            for idx, arr in enumerate(class_embeddings):
                real_idx = class_idxs[class_n][idx]
                embeddings[real_idx].append(arr)

    return np.array(embeddings, dtype=object), umap_arr, centroids, stds


activations = np.load(config.SAVED_DATA_PATH / "activations" / "x_val_activations.npy", allow_pickle=True)
y_preds = activations[:, -1]
class_idxs = {
    val: np.where((np.argmax([*y_preds], axis=1) == val) & (np.argmax(y_val, axis=1) == val))[0] for val in range(num_classes)
}

embeddings, umap_arr, centroids, stds = make_umap_arr(activations, class_idxs)


np.save(config.SAVED_DATA_PATH / "other" / "umap_arr.npy", umap_arr)
np.save(config.SAVED_DATA_PATH / "other" / "centroids.npy", centroids)
np.save(config.SAVED_DATA_PATH / "other" / "stds.npy", stds)
np.save(config.SAVED_DATA_PATH / "other" / "class_idxs_split.npy", class_idxs)

np.save(config.SAVED_DATA_PATH / "embeddings" / "x_val_emeddings.npy", embeddings)


def make_embeddings_adversarial(umap_arr, activations, y=None):
    """this is a helper function because making all the activations at once OOM errors

    Args:
        x ([type]): [description]
        y ([type]): [description]
        model ([type]): [description]
        umap_obj ([type], optional): [description]. Defaults to None.
    """

    y_preds = np.array([*activations[:, -1]])
    class_idxs_ = {val: np.where((np.argmax([*y_preds], axis=1) == val))[0] for val in range(num_classes)}
    # breakpoint()
    embeddings = [[] for _ in range(activations.shape[0])]

    for layer_n in trange(num_layers):
        # for layer_n in range(num_layers):
        umap_obj = umap_arr[layer_n]

        # we do this by class because when we do it for all the data, the embeddings are further spread out
        for class_n in range(num_classes):
            class_activations = transform_for_umap(activations[class_idxs_[class_n], layer_n])
            class_embeddings = umap_obj.transform(class_activations)

            for idx, arr in enumerate(class_embeddings):
                real_idx = class_idxs_[class_n][idx]
                embeddings[real_idx].append(arr)

    return np.array(embeddings)


print("==>making cw_val embeddings")
activations = np.load(config.SAVED_DATA_PATH / "activations" / "cw_x_val_activations.npy", allow_pickle=True)
embeddings = make_embeddings_adversarial(umap_arr, activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "cw_x_val_emeddings.npy", embeddings)

del activations
del embeddings

print("==>making fgsm_val embeddings")
activations = np.load(config.SAVED_DATA_PATH / "activations" / "fgsm_x_val_activations.npy", allow_pickle=True)
embeddings = make_embeddings_adversarial(umap_arr, activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "fgsm_x_val_emeddings.npy", embeddings)
