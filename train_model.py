from pathlib import Path
from typing import Callable, Tuple

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split

import config
from utils import data_init, load_data


def get_model(
    num_classes: int, base_model_func: Callable = tf.keras.applications.MobileNetV2, lr: float = 0.0001
) -> tf.keras.Model:
    input_layer = tf.keras.layers.Input(shape=config.IMG_SHAPE)
    base_model = base_model_func(include_top=False, input_shape=config.IMG_SHAPE, input_tensor=input_layer)
    base_model.trainable = False

    x = tf.keras.layers.GlobalAveragePooling2D()(base_model.output)
    classifier = tf.keras.layers.Dense(num_classes, activation="softmax")(x)
    model = tf.keras.Model(inputs=input_layer, outputs=classifier)

    loss_func = tf.keras.losses.CategoricalCrossentropy()
    model.compile(optimizer=tf.keras.optimizers.Adam(lr=lr), loss=loss_func, metrics=["accuracy"])
    return model


def train_model(
    model: tf.keras.Model, x: np.array, y: np.array, x_val: np.array = None, y_val: np.array = None, n_epochs: int = 100
):
    if (x_val == None) or (y_val == None):
        print("making validation data from training data")
        x, x_val, y, y_val = train_test_split(x, y, test_size=0.2)

    callbacks = [
        tf.keras.callbacks.EarlyStopping(patience=3),
    ]
    history = model.fit(
        x=x,
        y=y,
        epochs=n_epochs,
        batch_size=32,
        callbacks=callbacks,
        shuffle=True,
        validation_split=0.2,
        validation_data=(x_val, y_val),
    )

    return model, history


def load_model_(path=config.MODEL_PATH):
    return tf.keras.models.load_model(path)


def save_data(root, **kwargs):
    Path(root).mkdir(parents=True, exist_ok=True)

    for k, v in kwargs.items():
        np.save(f"{root}/{k}", v)


def get_models_available():
    from inspect import getmembers, isfunction

    models_available = getmembers(tf.keras.applications, isfunction)
    return models_available


if __name__ == "__main__":
    data_path = config.ORIGINAL_DATA_PATH
    model_path = config.MODEL_PATH
    datapath, _ = data_init(data_path)

    x, y, x_val, y_val = load_data(datapath)
    num_classes = y.shape[1]

    model = get_model(num_classes, base_model_func=tf.keras.applications.MobileNetV2)
    # model, history = train_model(model, x, y, x_val, y_val, n_epochs=100)
    model, history = train_model(model, x, y, n_epochs=100)
    model.save(model_path)