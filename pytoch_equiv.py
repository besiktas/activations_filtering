import torch
from torchvision import datasets, transforms
from torchvision.models import mobilenet_v2


import config


data_transforms = transforms.Compose(
    [
        transforms.Resize(config.IMG_SIZE),
        transforms.ToTensor(),
    ]
)

train_dataset = datasets.ImageFolder(config.ORIGINAL_DATA_PATH / "train", transform=data_transforms)

train_dataset, test_dataset = torch.utils.data.random_split(train_dataset, [len(train_dataset) - 50, 50])

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=4)

test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=32, shuffle=True, num_workers=4)


model_ft = mobilenet_v2(pretrained=True)

optimizer = torch.optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)


# def get_model():
#     pass


# def train_model(model, trainloader, n_epochs=100):

#     loss_func = torch.nn.CrossEntropyLoss()

#     for epoch in range(n_epochs):

#         for i, data in enumerate(trainloader):
#             x, y = data
#             optimizer.zero_grad()
#             outputs = model(x)
#             loss = loss_func(outputs, y)
#             optimizer.zero_grad()
#             loss.backward()


# def get_layer_outputs(model):
#     for layer in model.layers


# if __name__ == "__main__":
#     model = get_model()
