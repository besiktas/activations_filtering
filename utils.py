import json
from pathlib import Path
from typing import Dict, Tuple, Union

from fastai.data.all import URLs, get_image_files, untar_data
from fastcore.basics import Int
import numpy as np
from PIL import Image as pil_image
import tensorflow as tf

import config

# class Activations(object):
#     def __init__(self, )


class DataHelper(object):
    """
    filepath: original filepath of image

    saved_data_basefoldcer: folder where all the images will be put in their own subfolder
    new_filefolder: image saved as npy along with any generated images and metadata
    """

    def __init__(self, filepath: Path, saved_data_basefoldcer: Path) -> None:
        super().__init__()
        self.filepath = filepath
        self.saved_data_basefoldcer = saved_data_basefoldcer
        self.new_filefolder = self.saved_data_basefoldcer / f"{self.filepath.parts[-3]}_{self.filepath.parts[-1].split('.')[0]}"

        self.generated_data = {}
        self.input_data = {"x": None, "y": None}

        self.activations = {}

    @classmethod
    def from_original_filepath(cls, original_filepath, saved_data_basefolder):
        data = cls()

        # data.x =
        data.x = (img_as_array(original_filepath, target_size=config.IMG_SIZE) / 255.0,)
        data.y = config.labels_id_array.index(original_filepath.parts[-2])
        data.x_tensor = tf.convert_to_tensor([data.x], dtype=tf.float32)
        # data.new_filefolder = self.saved_data_basefoldcer / f"{self.filepath.parts[-3]}_{self.filepath.parts[-1].split('.')[0]}"
        return data

    @classmethod
    def from_saved_folder(self, filepath):
        pass

        # pass
        # self.new_filefolder = self.saved_data_basefoldcer / f"{self.filepath.parts[-3]}_{self.filepath.parts[-1].split('.')[0]}"

    # def load_

    # def load_original(self):

    #     self.x = img_as_array(self.filepath, target_size=config.IMG_SIZE) / 255.0,
    #     self.y = config.labels_id_array.index(self.filepath.parts[-2])

    #     # self.input_data = {
    #     #     "x": img_as_array(self.filepath, target_size=config.IMG_SIZE) / 255.0,
    #     #     "y": config.labels_id_array.index(self.filepath.parts[-2])
    #     # }

    #     self.x_tensor = tf.convert_to_tensor([self.input_data["x"]], dtype=tf.float32)

    def make_activations(self, model):
        func = tf.keras.backend.function([model.input], [model.layers[idx].output for idx in range(len(model.layers))])

        self.activations["x"] = func(self.input_data["x"])

        for attack_name, adv_x in self.generated_data.items():
            self.activations[attack_name] = func(adv_x)

    def generate_adversarial(self, func, model, name=None, **kwargs):
        if name is None:
            name = func.__name__
        self.generated_data[name] = func(model, self.x_tensor, **kwargs)

    def save_activations(self):
        for activation_name, activations in self.activations.items():
            np.save(self.new_filefolder / f"{activation_name}_activations.npy", activations)

    def save_adversarial(self):

        self.new_filefolder.mkdir(parents=True, exist_ok=True)

        np.save(self.new_filefolder / "data.npy", self.x)

        for key, value in self.generated_data.items():
            np.save(self.new_filefolder / key, value)

        with open(self.new_filefolder / "metadata.json", "w") as f:
            f.write(json.dumps({"y": self.y}))

    def load(self):
        pass


def data_init(dest=None):
    """
    helper func with fastai data
    """
    path = untar_data(URLs.IMAGENETTE_320, dest=dest)
    fnames = get_image_files(path)
    return path, fnames


def img_as_array(path: Union[Path, str], target_size: Tuple[int, int] = None) -> np.array:
    x = pil_image.open(path)

    # some images are grayscale
    if x.mode != "RGB":
        x = x.convert("RGB")
    if target_size:
        x = x.resize(target_size)

    return np.asarray(x)


def get_all_subfolder_images(base_folder: Path, img_size: Tuple[int, int] = config.IMG_SIZE) -> Tuple[np.array, np.array]:
    x = []
    y = []
    for idx, folder in enumerate(base_folder.iterdir()):
        x_ = [img_as_array(f, target_size=img_size) / 255 for f in folder.glob("*.JPEG")]

        y_ = [idx] * len(x_)
        x += x_
        y += y_

    assert len(x) == len(y)

    return np.asarray(x), np.asarray(y)


def load_data(path: Path) -> Tuple[np.array, np.array, np.array, np.array]:
    x, y = get_all_subfolder_images(path / "train")
    x_val, y_val = get_all_subfolder_images(path / "val")
    y = to_categorical(y)
    y_val = to_categorical(y_val)
    return x, y, x_val, y_val


def to_categorical(x: np.array) -> np.array:
    num_classes = x.max() + 1
    n_samples = x.shape[0]
    categorical = np.zeros((n_samples, num_classes))
    categorical[np.arange(n_samples), x] = 1
    return categorical


# if __name__ == "__main__":
#     import tensorflow as tf
#     data_path = config.ORIGINAL_DATA_PATH
#     model_path = config.MODEL_PATH
#     datapath, _ = data_init(data_path)

#     _, y = get_all_subfolder_images(datapath / "train")
#     # x, y, x_val, y_val = load_data(datapath)

#     # print(to_categorical(x1))
#     # print(tf.keras.utils.to_categorical(x1))
#     assert np.array_equal(to_categorical(y), tf.keras.utils.to_categorical(y))
