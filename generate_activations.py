# from centroid_filtering.config import SAVED_DATA_PATH
import multiprocessing
from multiprocessing import process
import numpy as np
import tensorflow as tf
from tensorflow.python.framework.tensor_conversion_registry import get
from tqdm import tqdm, trange
import umap

# import umap


from dataclasses import dataclass
import json
import itertools
from multiprocessing import Pool
from pathlib import Path
import os

import config


os.environ["CUDA_VISIBLE_DEVICES"] = "0"


def get_layer_output(x, model, idx=None):
    func = tf.keras.backend.function([model.input], [model.layers[idx].output for idx in range(len(model.layers))])

    # if i want a specific one i should
    if idx:
        func = tf.keras.backend.function([model.input], [model.layers[idx].output])
    return func(x)


class Activations(object):
    def __init__(self, activations):
        self.activations = activations

    # def


class Act(object):
    def __init__(
        self,
        # filepath,
        x=None,
        y=None,
        # classname_in_filepath=True,
    ):
        # self.filepath = filepath

        self.x = x
        self.y = y

    def generate_activation(self, model):
        self.layer_activations = get_layer_output(model, self.x)

    def calculate_distance_to_centroids(self, centroids):
        pass


def flatten(arr, num_vals=None):
    num_vals = len(arr) if not num_vals else num_vals
    return arr.reshape(num_vals, -1)


def load_original_images(path: Path):
    x_val = np.load(path / "original" / "x_val.npy")
    x_val_split = np.load(path / "original" / "x_val_split.npy")
    y_val = np.load(path / "original" / "y_val.npy")
    y_val_split = np.load(path / "original" / "y_val_split.npy")
    return x_val, x_val_split, y_val, y_val_split


def compute_centroid(activations, n_sigma=2.0):
    # filter outliers
    filtered_activations = activations[
        (abs(activations[:, 0] - np.mean(activations[:, 0])) < n_sigma * np.std(activations[:, 0]))
        & (abs(activations[:, 1] - np.mean(activations[:, 1])) < n_sigma * np.std(activations[:, 1]))
    ]

    centroid = filtered_activations.mean(axis=0)
    std = filtered_activations.std(axis=0)
    return centroid, std


def generate_activations(data, model):
    output_func = tf.keras.backend.function([model.input], [model.layers[idx].output for idx in range(len(model.layers))])

    activations = []
    for x in tqdm(data):
        act = output_func(np.expand_dims(x, axis=0))
        activations.append([arr.squeeze() for arr in act])

    return np.array(activations, dtype=object)


def should_filter_std(embedded_x, centroids, stds, y_pred, n_sigma=1):
    y_pred_class = y_pred.argmax()

    near_centroid_arr = []
    for layer_n in range(len(embedded_x)):

        x0 = embedded_x[layer_n]

        within_bounds = np.abs(x0 - centroids[y_pred_class][layer_n]) < n_sigma * stds[y_pred_class][layer_n][0]

        if False in within_bounds:
            near_centroid_arr.append(1)
        else:
            near_centroid_arr.append(0)
    #         print(near_centroid_arr, tmp)

    return near_centroid_arr


# if __name__ == "__main__":
class Dataloader(object):
    def __init__(self, base_folder: Path) -> None:
        super().__init__()
        self.base_folder = base_folder

    def load_all(self):
        with open(self.base_folder / "metadata.json", "r") as f:
            metadata = json.load(f)
        self.y = int(metadata["y"])

        self.x = self.base_folder / "data.npy"
        self.carlini_wagner_l2 = self.base_folder / "carlini_wagner_l2.npy"


# folders = [f for f in config.SAVED_DATA_PATH]

model = tf.keras.models.load_model(config.MODEL_PATH)
x_val, x_val_split, y_val, y_val_split = load_original_images(config.SAVED_DATA_PATH)

num_classes = y_val.shape[1]
num_layers = len(model.layers)

y_val = y_val.argmax(axis=1)
y_val_split = y_val_split.argmax(axis=1)

cw_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "cw_x_val.npy")
fgsm_x_val = np.load(config.SAVED_DATA_PATH / "adv" / "fgsm_x_val.npy")

cw_x_val = cw_x_val.squeeze()
fgsm_x_val = fgsm_x_val.squeeze()

# taking a subset here
N_SUBSET = config.OOM_SAMPLE_SIZE
cw_x_val = cw_x_val[:N_SUBSET]
fgsm_x_val = fgsm_x_val[:N_SUBSET]
x_val = x_val[:N_SUBSET]
x_val_split = x_val_split[:N_SUBSET]
y_val = y_val[:N_SUBSET]
y_val_split = y_val_split[:N_SUBSET]

# unpack_array = lambda arr: np.array([val.argmax() for val in arr])
unpack_array = lambda arr: np.argmax([*arr], axis=1)
transform_for_umap = lambda data: np.array([arr for arr in data]).reshape(len(data), -1)
get_final_activations = lambda act: np.array([*act[:, -1]])
# transform_for_umap = lambda data: np.array([*data]).reshape(data.shape[0], -1)


def make_umap_obj(layer_n):
    umap_obj = umap.UMAP(n_components=2, random_state=42, min_dist=0.0, transform_queue_size=6)
    return umap_obj


def make_class_idxs(y_true, y_preds):
    class_idxs = {val: np.where((y_preds == val) & (y_true == val))[0] for val in range(num_classes)}
    return class_idxs


def func_(args):
    layer_n = args[0]
    x_activations = args[1]
    # embeddings = args[2]
    # umap_arr = args[3]
    print(f"doing layer ...")

    embeddings_for_layer = []
    centroids_for_layer = []
    stds_for_layer = []

    umap_obj = umap.UMAP(random_state=42, min_dist=0.0)
    embedded_activations = umap_obj.fit_transform(transform_for_umap(x_activations[:, layer_n]))

    for sample_idx, val in enumerate(embedded_activations):
        embeddings.append(val)

    for class_n in range(num_classes):
        class_activations = embedded_activations[class_idxs[class_n], :]
        centroid, std = compute_centroid(class_activations)
        centroids_for_layer.append(centroid)
        stds_for_layer.append(std)
        # centroids[class_n][layer_n] = centroid
        # stds[class_n][layer_n] = std
    # umap_arr[layer_n] = umap_obj
    return layer_n, umap_obj, centroids_for_layer, embeddings


def make_umap_objs(x_activations, y, class_idxs):

    centroids = {val_c: [[] for val_l in range(num_layers)] for val_c in range(num_classes)}
    std = {val_c: [[] for val_l in range(num_layers)] for val_c in range(num_classes)}

    umap_arr = {val: None for val in range(num_layers)}

    # if config.USE_MP:
    #     embeddings = [[[] for _ in range(x_activations.shape[1])] for _ in range(x_activations.shape[0])]

    #     with multiprocessing.Pool(processes=10) as pool:
    #         mp_iterable = zip(
    #             range(num_layers),
    #             itertools.repeat(x_activations),
    #         )
    #         arr = pool.map(func_, mp_iterable)

    embeddings = [[] for _ in range(x_activations.shape[0])]
    for layer_n in trange(num_layers):
        # i need to do something more extensive with the parameter searching here
        umap_obj = umap.UMAP(
            random_state=42,
            min_dist=0.0,
            # n_components=2,
            # transform_queue_size=6
        )

        embedded_activations = umap_obj.fit_transform(transform_for_umap(x_activations[:, layer_n]))
        umap_arr.append(umap_obj)

        for idx, val in enumerate(embedded_activations):
            embeddings[idx].append(val)

        # for each class in normal activations, calculate centroid
        for class_n in range(num_classes):
            class_activations = embedded_activations[class_idxs[class_n], :]
            centroid, std = compute_centroid(class_activations)

            # centroids[class_n].append(centroid)
            # stds[class_n].append(std)
            centroids[class_n][layer_n] = centroid
            stds[class_n][layer_n] = std

    embeddings = np.array(embeddings)

    return umap_arr, centroids, stds, embeddings


def make_embeddings(umap_arr, x_activations):
    """this is a helper function because making all the activations at once OOM errors

    Args:
        x ([type]): [description]
        y ([type]): [description]
        model ([type]): [description]
        umap_obj ([type], optional): [description]. Defaults to None.
    """

    y_preds = unpack_array(x_activations[:, -1])
    class_idxs = {val: np.where(y_preds == val)[0] for val in range(num_classes)}

    embeddings = [[] for _ in range(x_activations.shape[0])]

    # def func_(umap_obj, class_n, layer_n):

    def func_(args):
        class_n = args[0]
        layer_n = args[1]
        umap_obj = umap_arr[layer_n]
        # umap_obj = args[0]
        class_activations = transform_for_umap(x_activations[class_idxs[class_n], layer_n])
        class_embeddings = umap_obj.transform(class_activations)

        for idx, arr in enumerate(class_embeddings):
            embeddings[class_idxs[class_n][idx]].append(arr)

    for layer_n in trange(num_layers):
        umap_obj = umap_arr[layer_n]

        for class_n in range(num_classes):
            class_activations = transform_for_umap(x_activations[class_idxs[class_n], layer_n])
            class_embeddings = umap_obj.transform(class_activations)

            for idx, arr in enumerate(class_embeddings):
                real_idx = class_idxs[class_n][idx]
                embeddings[real_idx].append(arr)


print("==>making umap_obj/centroids/class_idxs with x_val_split")
print("==>We Assume we have access to this data and it doesnt have attacks")

activations = generate_activations(x_val_split, model)
x_val_split_final_activations = get_final_activations(activations)
class_idxs_split = make_class_idxs(y_val_split, unpack_array(activations[:, -1]))

print(f"==>making umap_objs")
umap_arr, centroids, stds, x_val_split_embeddings = make_umap_objs(activations, y_val_split, class_idxs_split)

print(f"==>embeddings shape: {x_val_split_embeddings.shape}")

print(f"==>saving act, embed etc...")
np.save(config.SAVED_DATA_PATH / "activations" / "x_val_split_activations.npy", activations)
np.save(config.SAVED_DATA_PATH / "activations" / "x_val_split_activations.npy", activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "x_val_split_embeddings.npy", x_val_split_embeddings)

# this otherwise oom
import gc

del activations
gc.collect()
gc.collect()
umap_arr = np.array(umap_arr, dtype=object)
print(f"==>saving umap_arr etc...")
np.save(config.SAVED_DATA_PATH / "other" / "umap_arr.npy", umap_arr)

print(f"==>saving centroids, stds, class_idxs_split...")
# np.save(config.SAVED_DATA_PATH / "other" / "umap_arr.npy", umap_arr)
np.save(config.SAVED_DATA_PATH / "other" / "centroids.npy", centroids)
np.save(config.SAVED_DATA_PATH / "other" / "stds.npy", stds)
np.save(config.SAVED_DATA_PATH / "other" / "class_idxs_split.npy", class_idxs_split)

print("done with x_val_split\n=== === ===")


print("==>making x_val activations")
activations = generate_activations(x_val, model)
x_val_final_activations = get_final_activations(activations)
class_idxs = make_class_idxs(y_val, unpack_array(activations[:, -1]))
x_val_embeddings = make_embeddings(umap_arr, activations)
np.save(config.SAVED_DATA_PATH / "activations" / "x_val_activations.npy", activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "x_val_embeddings.npy", x_val_embeddings)
np.save(config.SAVED_DATA_PATH / "other" / "class_idxs.npy", class_idxs)

print("==>making cw_val activations")
activations = generate_activations(cw_x_val, model)
cw_x_val_final_activations = get_final_activations(activations)
cw_x_val_embeddings = make_embeddings(umap_arr, activations)
np.save(config.SAVED_DATA_PATH / "activations" / "cw_x_val_activations.npy", activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "cw_x_val_embeddings.npy", cw_x_val_embeddings)

print("==>making fgsm activations")
activations = generate_activations(fgsm_x_val, model)
fgsm_x_val_final_activations = get_final_activations(activations)
fgsm_x_val_embeddings = make_embeddings(umap_arr, activations)
np.save(config.SAVED_DATA_PATH / "activations" / "fgsm_x_val_activations.npy", activations)
np.save(config.SAVED_DATA_PATH / "embeddings" / "fgsm_x_val_embeddings.npy", fgsm_x_val_embeddings)


# if i want to use a class maybe something like ?
# activations = Activations(x_val)
# activations.generate(model)
# umap_arr = activations.make_umap()
# activations.save()

# for folder in config.SAVED_DATA_PATH.iterdir():
#     data = Dataloader(folder)
#     data.make_activations()
#     data.save_activations()
