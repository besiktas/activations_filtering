# from helpers_data import to_categorical
# from pathlib import Path
# from typing import Tuple

# from PIL import Image as pil_image
# import numpy as np
# import tensorflow as tf
# from fastai.data.all import URLs, get_image_files, untar_data
# from tqdm import tqdm


# from cleverhans.tf2.attacks.fast_gradient_method import fast_gradient_method
# from cleverhans.tf2.attacks.carlini_wagner_l2 import CarliniWagnerL2



# import config


# def generate_activations(model, example):
#     pass


# def data_init(dest=None):
#     """
#     helper func with fastai data
#     """
#     path = untar_data(URLs.IMAGENETTE_320, dest=dest)
#     fnames = get_image_files(path)
#     return path, fnames


# def img_as_array(path, target_size: Tuple[int, int] = None):
#     x = pil_image.open(path)

#     # some images are grayscale
#     if x.mode != "RGB":
#         x = x.convert("RGB")
#     if target_size:
#         x = x.resize(target_size)

#     return np.asarray(x)


# def get_all_subfolder_images(base_folder, img_size=config.IMG_SIZE):

#     x = []
#     y = []
#     for idx, folder in enumerate(base_folder.iterdir()):
#         x_ = [img_as_array(f, target_size=img_size)/255 for f in folder.glob("*.JPEG")]

#         y_ = [idx] * len(x_)
#         x += x_
#         y += y_

#     assert len(x) == len(y)

#     return np.asarray(x), np.asarray(y)


# def load_data(path):
#     x, y = get_all_subfolder_images(path / "train")
#     x_val, y_val = get_all_subfolder_images(path / "val")
#     y = tf.keras.utils.to_categorical(y)
#     y_val = tf.keras.utils.to_categorical(y_val)
#     return x, y, x_val, y_val


# def get_model(num_classes, base_model_func=tf.keras.applications.MobileNetV2, lr=0.0001):
#     input_layer = tf.keras.layers.Input(shape=config.IMG_SHAPE)
#     base_model = base_model_func(include_top=False, input_shape=config.IMG_SHAPE, input_tensor=input_layer)
#     base_model.trainable = False

#     x = tf.keras.layers.GlobalAveragePooling2D()(base_model.output)
#     classifier = tf.keras.layers.Dense(num_classes, activation="softmax")(x)
#     model = tf.keras.Model(inputs=input_layer, outputs=classifier)

#     loss_func = tf.keras.losses.CategoricalCrossentropy()
#     model.compile(optimizer=tf.keras.optimizers.Adam(lr=lr), loss=loss_func, metrics=["accuracy"])
#     return model


# def train_model(model, x, y, x_val, y_val, n_epochs=100):
#     callbacks = [
#         tf.keras.callbacks.EarlyStopping(patience=3),
#     ]
#     history = model.fit(
#         x=x,
#         y=y,
#         epochs=n_epochs,
#         batch_size=32,
#         callbacks=callbacks,
#         shuffle=True,
#         validation_data=(x_val, y_val),
#     )

#     return model, history


# def load_model_(path=config.MODEL_PATH):
#     return tf.keras.models.load_model(path)


# def save_data(root, **kwargs):
#     Path(root).mkdir(parents=True, exist_ok=True)

#     for k, v in kwargs.items():
#         np.save(f"{root}/{k}", v)


# def get_models_available():
#     from inspect import getmembers, isfunction

#     models_available = getmembers(tf.keras.applications, isfunction)
#     return models_available

# def cleverhans_helper_cw2(model, x):
#     """data has to be single instance and """
#     adv_x = []
#     attacker = CarliniWagnerL2(model)

#     for idx in range(len(x)):
#         x_tensor = tf.cast(np.expand_dims(x[idx], axis=0), tf.float32)
#         adv_x.append(attacker.attack(x_tensor))

#     return np.asarray(adv_x), attacker




# if __name__ == "__main__":
#     data_path = config.ORIGINAL_DATA_PATH
#     datapath, _ = data_init(config.ORIGINAL_DATA_PATH)

#     x, y, x_val, y_val = load_data(datapath)
#     num_classes = y.shape[1]

#     model = get_model(num_classes, base_model_func=tf.keras.applications.MobileNetV2)
#     model, history = train_model(model, x, y, x_val, y_val, n_epochs=100)
#     model.save(config.MODEL_PATH)



#     # adversarial images from cw2, this attack is incredibly slow!
#     # adv_cw_x, attacker = cleverhans_helper_cw2(model, x)
#     # adv_cw_x_val, _ = cleverhans_helper_cw2(model, x_val)

#     # # fgsm attacks
#     # adv_fgsm_x = fast_gradient_method(model, tf.convert_to_tensor(x, dtype=tf.float32), eps=1.0, norm=np.inf)
#     # adv_fgsm_x_val = fast_gradient_method(model, tf.convert_to_tensor(x_val, dtype=tf.float32), eps=1.0, norm=np.inf)


#     # save_data(
#     #     root=config.SAVED_DATA_PATH / "adv",
#     #     adv_cw_x=adv_cw_x,
#     #     adv_cw_x_val=adv_cw_x_val,
#     #     adv_fgsm_x=adv_fgsm_x,
#     #     adv_fgsm_x_val=adv_fgsm_x_val,
#     # )


#     # save_data(
#     #     root=config.SAVED_DATA_PATH / "original",
#     #     x=x,
#     #     y=y,
#     #     x_val=x_val,
#     #     y_val=y_val,
#     # )
