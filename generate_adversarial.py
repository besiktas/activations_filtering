import argparse
from dataclasses import dataclass
from pathlib import Path
import os

import tensorflow as tf
import numpy as np

from cleverhans.tf2.attacks.fast_gradient_method import fast_gradient_method
from cleverhans.tf2.attacks.carlini_wagner_l2 import carlini_wagner_l2, CarliniWagnerL2
from tqdm import tqdm
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

import config
from utils import data_init, Dataloader, load_data


os.environ["CUDA_VISIBLE_DEVICES"] = "1"


def adversarial_helper(model, x, y, path, prefix):
    """
    function that creates adversarial examples for each sample in x
    then saves individually since other method is slow we can use
    """

    cw_attacker = CarliniWagnerL2(model)

    cw_path = path / "adv" / "cw"
    cw_path.mkdir(parents=True, exist_ok=True)

    fgsm_path = path / "adv" / "fgsm"
    fgsm_path.mkdir(parents=True, exist_ok=True)

    # since we shuffle the y save it

    cw_x = []
    fgsm_x = []

    for idx in tqdm(range(len(x))):

        y_class = y[idx].argmax()

        x_tensor = tf.convert_to_tensor([x[idx]], dtype=tf.float32)

        # put any attacks you want to generate here
        x_cw_adv = cw_attacker.attack(x_tensor)
        x_fgsm_adv = fast_gradient_method(model, x_tensor, eps=0.1, norm=np.inf)

        cw_x.append(x_cw_adv)
        fgsm_x.append(x_fgsm_adv)

        # np.save(cw_path / f"{prefix}_{y_class}_{idx}", x_cw_adv)
        # np.save(fgsm_path / f"{prefix}_{y_class}_{idx}", x_fgsm_adv)
    return np.asarray(cw_x), np.asarray(fgsm_x)
    # return


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int, default=config.OOM_SAMPLE_SIZE)
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--val", action="store_true")
    return parser.parse_args()


def get_datafilenames(path):
    pass


if __name__ == "__main__":
    model = tf.keras.models.load_model(config.MODEL_PATH)

    # datapath, fnames = data_init(config.ORIGINAL_DATA_PATH)
    # val_fnames = [val for val in fnames if val.parts[6] is "val"]

    # x_val_filepaths, x_val_split_filepaths = train_test_split(val_fnames, test_size=0.2)

    # for x in x_val_filepaths:
    #     data = Dataloader(x, config.SAVED_DATA_PATH)

    #     data.generate_adversarial(carlini_wagner_l2, model)
    #     data.generate_adversarial(fast_gradient_method, model, eps=0.1, norm=np.inf)
    #     data.save()

    # testing above

    args = get_args()

    datapath, _ = data_init(config.ORIGINAL_DATA_PATH)

    x_train, y_train, x_val, y_val = load_data(datapath)

    # create split and we shuffle here, split may be used later
    x_val, x_val_split, y_val, y_val_split = train_test_split(x_val, y_val, test_size=0.5)

    # x_val_split = x_val_split[: args.n]
    # y_val_split = y_val_splti[: args.n]

    x_val = x_val[: args.n]
    y_val = y_val[: args.n]

    print("creating adversarial data")
    # cw_x_val, fgsm_x_val = adversarial_helper(model, x_val, y_val, config.SAVED_DATA_PATH, prefix="val")
    cw_attacker = CarliniWagnerL2(model)

    cw_path = config.SAVED_DATA_PATH / "adv" / "cw"
    fgsm_path = config.SAVED_DATA_PATH / "adv" / "fgsm"

    cw_path.mkdir(parents=True, exist_ok=True)
    fgsm_path.mkdir(parents=True, exist_ok=True)

    # since we shuffle the y save it

    cw_x_val = []
    fgsm_x_val = []

    for idx in tqdm(range(len(x_val))):

        y_class = y_val[idx].argmax()

        x_tensor = tf.convert_to_tensor([x_val[idx]], dtype=tf.float32)

        # put any attacks you want to generate here
        x_cw_adv = cw_attacker.attack(x_tensor)
        x_fgsm_adv = fast_gradient_method(model, x_tensor, eps=0.1, norm=np.inf)

        cw_x_val.append(x_cw_adv)
        fgsm_x_val.append(x_fgsm_adv)

    cw_x_val = np.array(cw_x_val)
    fgsm_x_val = np.array(fgsm_x_val)
    breakpoint()

    cw_val = model.evaluate(cw_x_val, y_val)
    fgsm_val = model.evaluate(fgsm_x_val, y_val)
    print(
        f"evaluating the attacks, lower means attack was more successful"
        + "\n==>cw attacks val: {cw_val}\n==>fgsm attacks val: {fgsm_val}"
    )

    print(f"~~~saving data~~~")

    np.save(config.SAVED_DATA_PATH / "adv" / "cw_x_val.npy", cw_x_val)
    np.save(config.SAVED_DATA_PATH / "adv" / "fgsm_x_val.npy", fgsm_x_val)

    # print("creating adversarials for validation data")
    # adversarial_helper(model, x_val, y_val, config.SAVED_DATA_PATH, prefix="x_val")

    print("saving original x_val, x_val_split if needed")
    np.save(config.SAVED_DATA_PATH / "original" / "x_val", x_val)
    np.save(config.SAVED_DATA_PATH / "original" / "y_val", y_val)

    np.save(config.SAVED_DATA_PATH / "original" / "x_val_split", x_val_split)
    np.save(config.SAVED_DATA_PATH / "original" / "y_val_split", y_val_split)
