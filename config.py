from pathlib import Path
from dataclasses import dataclass

USE_MP = True

# PATHS
MODEL_PATH = Path("/data/graham/clustering/models/latest")
SAVED_DATA_PATH = Path("/data/graham/clustering/data/latest")

ORIGINAL_DATA_PATH = Path("/data/graham/data/fastai")
SAVED_PLOTS = Path("/data/graham/clustering/plots")
# SAVED_DATA

# use this value unless i refactor how im generating activations to not be 1 np array
OOM_SAMPLE_SIZE = 200

# image/model related
BATCH_SIZE = 32
IMG_SIZE = (320, 320)
IMG_SHAPE = IMG_SIZE + (3,)


labels = {
    "n01440764": "tench",
    "n02102040": "English springer",
    "n02979186": "cassette player",
    "n03000684": "chain saw",
    "n03028079": "church",
    "n03394916": "French horn",
    "n03417042": "garbage truck",
    "n03425413": "gas pump",
    "n03445777": "golf ball",
    "n03888257": "parachute",
}

labels_id_array = list(labels.keys())
labels_array = list(labels.values())


@dataclass
class Labels:
    labels = {
        "n01440764": "tench",
        "n02102040": "English springer",
        "n02979186": "cassette player",
        "n03000684": "chain saw",
        "n03028079": "church",
        "n03394916": "French horn",
        "n03417042": "garbage truck",
        "n03425413": "gas pump",
        "n03445777": "golf ball",
        "n03888257": "parachute",
    }

    def __post_init__(self):
        self.labels_array = list(self.labels.values())

    @staticmethod

    # def __getitem__(self, key: int):
    #     return self._labels_array[key]

    def classname_to_id(self, s):
        return self.labels

    def keys(self):
        return self.labels.keys()

    def items(self):
        return self.labels.items()
